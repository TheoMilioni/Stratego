Stratego - Projet pour l'UE LIFAP4
==================================

Code C++
Libs : SDL2, SDL_Image, SDL_TTF, SDL_Mixer
Compilation avec Makefile (+ projet Codeblocks)


Groupe
------

Théo Milioni		1*******
Alan Guyomarc'h		11509425
Léo Paquet			11510585


Manuel
------

Adaptation du jeu de société 'Stratego' édité par Hasbro
Mode console (stratego_txt) et graphique (stratego_sdl)

En mode texte, prompt pour chaque action
En mode graphique, action à la souris et touche ENTREE pour passer son tour


Organisation des fichiers
-------------------------

Stratego
	bin/
		debug
		stratego_txt
		stratego_sdl
	data/
		... (assets du programme)
	doc/
		Doxyfile
		cahier_charges.pdf
		gantt.pdf
		uml.dia
		uml.png
	obj/
		... (fichiers objets créés à la compilation)
	src/
		*.cpp
		*.h
	Makefile
	readme.txt


Compilation
-----------

$ make 			: génère les 3 éxécutables
$ make $name 	: génère l'éxecutable $name dans {debug, stratego_txt, stratego_sdl}
$ make clean 	: supprime les fichiers objets, les éxécutables et la documentation html
$ make doc		: génère la documentation et l'ouvre (avec firefox)
$ make tarball  : génère une archive (.tar.gz) du projet

