CC = g++
LD = g++
CFLAGS = -Wall -ggdb -std=c++11
INCLUDES = -I/usr/include
LDFLAGS =
LIBS = -L/usr/lib

SRC_DIR = src
OBJ_DIR = obj
BIN_DIR = bin

SRC_CORE = Game.cpp Player.cpp Piece.cpp Vector2.cpp
SRC_DEBUG = $(SRC_CORE) main.cpp
SRC_TXT = $(SRC_CORE) mainTxt.cpp Txt_Frontend.cpp
SRC_SDL = $(SRC_CORE) mainSDL2.cpp SDL2_Frontend.cpp

ifeq ($(OS),Windows_NT)
	INCLUDES_SDL = 	-Iextern/SDL2_mingw/SDL2-2.0.3/include \
				-Iextern/SDL2_mingw/SDL2_ttf-2.0.12/i686-w64-mingw32/include/SDL2 \
				-Iextern/SDL2_mingw/SDL2_image-2.0.0/i686-w64-mingw32/include/SDL2 \
				-Iextern/SDL2_mingw/SDL2_mixer-2.0.0/i686-w64-mingw32/include/SDL2
	LIBS_SDL = 	-Lextern \
							-Lextern/SDL2_mingw/SDL2-2.0.3/i686-w64-mingw32/lib \
							-Lextern/SDL2_mingw/SDL2_ttf-2.0.12/i686-w64-mingw32/lib \
							-Lextern/SDL2_mingw/SDL2_image-2.0.0/i686-w64-mingw32/lib \
							-Lextern/SDL2_mingw/SDL2_mixer-2.0.0/i686-w64-mingw32/lib \
							-lmingw32 -lSDL2main -lSDL2.dll -lSDL2_ttf.dll -lSDL2_image.dll -lSDL2_mixer.dll
else
	INCLUDES_SDL = -I/usr/include/SDL2
	LIBS_SDL = $(LIBS) -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
endif

OUT_DEBUG = debug
OUT_TXT = stratego_txt
OUT_SDL = stratego_sdl

all: debug txt sdl

debug: mkdir $(BIN_DIR)/$(OUT_DEBUG)
txt: mkdir $(BIN_DIR)/$(OUT_TXT)
sdl: mkdir $(BIN_DIR)/$(OUT_SDL)

$(BIN_DIR)/$(OUT_DEBUG): $(SRC_DEBUG:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS) $(LIBS)

$(BIN_DIR)/$(OUT_TXT): $(SRC_TXT:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS) $(LIBS)

$(BIN_DIR)/$(OUT_SDL): $(SRC_SDL:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS) $(LIBS_SDL)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) $(INCLUDES) $(INCLUDES_SDL) -o $@ -c $<

mkdir:
	mkdir -p bin obj data doc src

clean:
	@echo "Cleaning workspace"
	rm -f obj/*.o
	rm -f bin/*
	rm -rf doc/html
	rm -f vgcore.*

doc:
	@echo "Creating documentation"
	doxygen doc/Doxyfile
	firefox doc/html/index.html
	
tarball: clean
	@echo "Exporting tarball (except docker image)"
	tar -cvzf STRATEGO_MILIONI_GUYOMARCH_PAQUET.tar.gz --exclude='.hg/*' *
	
.PHONY: doc
