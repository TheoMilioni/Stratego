#include "Player.h"

#include <iostream>
#include <algorithm>
#include <cassert>
#include "Piece.h"

Player::Player() {
    name = "IA";
}

Player::Player(std::string _name) {
    name = _name;
}

Player::~Player() {
    // Libère les pointeurs des pieces
    for (unsigned int i = 0; i < pieces.size(); i++) {
        delete pieces[i];
        pieces[i] = NULL;
    }
}

std::string Player::getName() const {
    return name;
}

void Player::fillPieces(mode m) {
    Vector2 invalidPosition(-1, -1);
    int i;

    if (m == normal) {
        pieces.push_back(new Flag(invalidPosition, this));
        for (i = 0; i < 6/*6*/; i++) pieces.push_back(new Bomb(invalidPosition, this));
        pieces.push_back(new Piece(10, invalidPosition, this));
        pieces.push_back(new Piece(9, invalidPosition, this));
        for (i = 0; i < 2/*2*/; i++) pieces.push_back(new Piece(8, invalidPosition, this));
        for (i = 0; i < 3;/*3*/i++) pieces.push_back(new Piece(7, invalidPosition, this));
        for (i = 0; i < 4;/*4*/i++) pieces.push_back(new Piece(6, invalidPosition, this));
        for (i = 0; i < 4;/*4*/ i++) pieces.push_back(new Piece(5, invalidPosition, this));
        for (i = 0; i < 4;/*4*/ i++) pieces.push_back(new Piece(4, invalidPosition, this));
        for (i = 0; i < 5;/*5*/ i++) pieces.push_back(new Sapper(invalidPosition, this));
        for (i = 0; i < 8;/*8*/ i++) pieces.push_back(new Scout(invalidPosition, this));
        pieces.push_back(new Spy(invalidPosition, this));

        assert(pieces.size()==40); // 5
    }
    else if (m == fast) {
        pieces.push_back(new Piece(10, invalidPosition, this));
        pieces.push_back(new Piece(9, invalidPosition, this));
        for (i = 0; i < 2; i++) pieces.push_back(new Piece(3, invalidPosition, this));
        for (i = 0; i < 2; i++) pieces.push_back(new Piece(2, invalidPosition, this)); // erreur : il y avait 3 au lieu de 2
        pieces.push_back(new Piece(1, invalidPosition, this));
        for (i = 0; i < 2; i++) pieces.push_back(new Bomb(invalidPosition, this));
        pieces.push_back(new Flag(invalidPosition, this));
    }
    else if (m == debug) {
        pieces.push_back(new Piece(10, invalidPosition, this));
        pieces.push_back(new Spy(invalidPosition, this));
        pieces.push_back(new Flag(invalidPosition, this));
        pieces.push_back(new Scout(invalidPosition, this));
        pieces.push_back(new Scout(invalidPosition, this));
        pieces.push_back(new Sapper(invalidPosition, this));
        pieces.push_back(new Bomb(invalidPosition, this));
    }
}

int Player::getNumberOfPieces() const {
    return pieces.size();
}

Piece* Player::getPiece(int index) const {
    assert((unsigned int) index < pieces.size());

    return pieces[index];
}

void Player::removePiece(Piece* piece) {
    pieces.erase(std::remove(pieces.begin(), pieces.end(), piece), pieces.end());
    delete piece;
    piece = NULL;
}

bool Player::hasFlag() const {
    for (unsigned int i = 0; i < pieces.size(); i++) {
        if (pieces[i]->getValue() == 12) {
            return true;
        }
    }
    return false;
}

bool Player::hasLost() const {
    return(getNumberOfPieces() == 1
           || !(hasFlag()));
}

bool Player::operator==(const Player& player) const {
    return this == &player;
}

bool Player::operator!=(const Player& player) const {
    return !(this == &player);
}

void Player::regression() {
    Player alice("Alice");
    Player* bob = new Player("Bob");

    // Tests d'égalité
    assert (alice != *bob);
    assert (alice == alice);

    // verifie si le destructeur libère bien la mémoire
    // (avec valgrind pour vérifier fuites)
    bob->fillPieces(normal);
    Piece* piece =bob->getPiece(2);
    assert(piece != NULL);
    delete bob;
}
