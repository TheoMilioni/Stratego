#ifndef INCLUDE_VECTOR2
#define INCLUDE_VECTOR2

/** @brief Classe Vector2
 *
 * Représente une position 2D (x, y)
 */
class Vector2 {
public:
    int x;  /**< Coordonnées en x */
    int y;  /**< Coordonnées en y */

    Vector2(void);
    Vector2(const int x, const int y);
    Vector2(const Vector2 &v);

    Vector2& operator=(const Vector2 &v);
    bool operator ==(const Vector2 &v) const;
    bool operator !=(const Vector2 &v) const;
};

#endif
