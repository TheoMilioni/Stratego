#ifndef INCLUDE_TXT_FRONTEND
#define INCLUDE_TXT_FRONTEND

#include <string>
#include <vector>
#include "Game.h"
#include "Enums.h"

class Player;

/** @brief Classe Txt_Frontend, gère l'affichage en console de l'app.
 */
class Txt_Frontend {
private:
    Game* game; /**< Pointeur sur l'objet jeu */

public:
    /** @brief Constructeur de Txt_Frontend
    *
    */
    Txt_Frontend();



    /** @brief Destructeur de Txt_Frontend
    *
    */
    ~Txt_Frontend();



    /** @brief Démarre une partie en mode normal
    *
    * @param m = normal mode
    * @return void
    *
    */
    void start(mode m = normal);



    /** @brief Test de regression du module Txt_Frontend
    *
    * @return void
    *
    */
    void regression();

private:
    /** @brief Boucle de jeu
    *
    * @return void
    *
    */
    void loop();



    /** @brief Affiche un message spécifiant le joueur vainqueur
    *
    * @param winner Player*
    * @return void
    *
    */
    void endGame(Player* winner);



    /** @brief Affiche le plateau
    *
    * @param player const Player*
    * @return void
    *
    */
    void drawBoard(const Player* player);



    /** @brief Demande les coordonnées d'un pion au joueur
    * afin de placer ce pion sur le plateau
    *
    * @param player Player*
    * @return void
    *
    */
    void placePieces(Player* player);



    /** @brief Permet de vérifier si la sélection d'un pion
    * est possible, puis sélectionne le pion
    *
    * @param player const Player*
    * @return Piece*
    *
    */
    Piece* selectPiece(const Player* player);



    /** @brief Permet de vérifier si l'attaque d'un pion
    * d'un joueur adverse est possible, et attaque
    * le pion
    * @param player Player*
    * @return void
    *
    */
    void doAttack(Player* player);



    /** @brief Permet de vérifier si le déplacement d'un
    * pion est possible, et déplace le pion
    *
    * @param player Player*
    * @return void
    *
    */
    void doMove(Player* player);



    /** @brief efface la fenêtre graphique
    *
    * @return void
    *
    */
    void clearScreen() const;



    /** @brief reset std::cin
    *
    * @return void
    *
    */
    void clearCin() const;



    /** @brief Permet de saisir des coordonnées valides
    * dans le plateau
    *
    * @return Vector2
    *
    */
    Vector2 promptCoordinates() const;



    /** @brief Affiche un prompt booléen (oui/ non)
    *
    * @param msg std::string
    * @param yesDefault = true bool
    * @return bool
    *
    */
    bool promptBool(std::string msg, bool yesDefault = true) const;



    /** @brief Affiche un menu et retourne l'entrée choisie
    *
    * @param nbOptions unsigned int
    * @param title std::string
    * @param options std::vector<std::string>
    * @return int
    *
    */
    int promptMenu(unsigned int nbOptions, std::string title, std::vector<std::string> options) const;
};

#endif
