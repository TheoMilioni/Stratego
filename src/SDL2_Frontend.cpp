#include "SDL2_Frontend.h"
#include <iostream>
#include <unistd.h>
#include <string>
#include <sstream>
#include <cassert>
#include "Game.h"
#include "Player.h"
#include "Piece.h"

/******************************************************************************/
/** Structures */
/******************************************************************************/

void State::setOpponent(int i, Piece* piece) {
    // Indice correct
    assert(i == 0 || i == 1);

    delete opponents[i];
    opponents[i] = piece;
}

void State::clean() {
    movingPiece = false;
    moveDone = false;
    /* Supprimé pour conserver affichage attaque entre les tours
    attackDone = false;
    for (int i = 0; i < 2; i++) {
        delete opponents[i];
        opponents[i] = NULL;

    }*/
    clickedSquare = nullVector;
}

bool Button::onButton(int x, int y) {
    const bool res =
        x >= destRect.x
        && x <= destRect.x + destRect.w
        && y >= destRect.y
        && y <= destRect.y + destRect.h;

    return res;
}

/******************************************************************************/
/** Constructeur et Destructeur */
/******************************************************************************/

SDL2_Frontend::SDL2_Frontend() {
    // Initialisation SDL2
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        std::cerr << "Erreur lors de l'initialisation de SDL2 : "
                  << SDL_GetError()
                  << std::endl;
        SDL_Quit();
        exit(1);
    }

    // Initialisation SDL2_Image
    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        std::cerr << "Erreur lors de l'initialisation de SDL_Image : "
                  << IMG_GetError()
                  << std::endl;
        SDL_Quit();
        exit(1);
    }

    // Initialise SDL2 ttf
    if (TTF_Init() == -1) {
        std::cerr << "Erreur lors de l'initialisation de SDL_TTF : "
                  << TTF_GetError()
                  << std::endl;
        SDL_Quit();
        exit(1);
    }

    // Initialise SDL_mixer
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 ) {
        std::cerr << "Erreur lors de l'initialisation de SDL_mixer : "
                  << Mix_GetError()
                  << std::endl;
        SDL_Quit();
        exit(1);
    }

    // Création de la fenêtre principale
    window = SDL_CreateWindow("Stratego",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              624,
                              444,
                              SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Erreur lors de la creation de la fenetre : "
                  << SDL_GetError()
                  << std::endl;
        SDL_Quit();
        exit(1);
    }

    // Création du renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        std::cerr << "Erreur lors de la creation du renderer : "
                  << SDL_GetError()
                  << std::endl;
        SDL_Quit();
        exit(1);
    }

    // Ouverture des polices
    globalFont = TTF_OpenFont("data/roboto.ttf", 20);
    menuFont = TTF_OpenFont("data/OldLondon.ttf", 40);

    // Chargement des textures
    loadPiecesTextures();
    boardTexture = loadImage("data/map_reserve.bmp");
    logoTexture = loadImage("data/logo.png");
    menuTexture = loadImage("data/parchemin.jpg");

    //initialise State
    //movingPiece;attackDone;moveDone;opponent1;opponent2;clickedSquare;
    state = {false, false, false, NULL, NULL, Vector2(-1, -1)};

    // Charge les sons du jeu
    musicMenu = loadMusic("data/in_aeris.mp3");
    musicGame = loadMusic("data/Guerre-Amb3.mp3");
    musicVictory = loadMusic("data/music-victory-fanfare.mp3");
    battleSound = loadSoundEffect("data/punches.wav");

}

SDL2_Frontend::~SDL2_Frontend() {
    // Libère les textures
    SDL_DestroyTexture(boardTexture);
    SDL_DestroyTexture(logoTexture);
    for (unsigned int i = 0; i < redPieces.size(); i++) {
        SDL_DestroyTexture(redPieces[i]);
    }
    for (unsigned int i = 0; i < bluePieces.size(); i++) {
        SDL_DestroyTexture(bluePieces[i]);
    }

    // Libère la structure state
    state.clean();
    for (int i = 0; i < 2; i++) {
        state.setOpponent(i, NULL);

    }

    // Libère les sons
    Mix_FreeMusic(musicMenu);
    Mix_FreeMusic(musicGame);
    Mix_FreeMusic(musicVictory);
    Mix_FreeChunk(battleSound);

    // Détruit le renderer
    SDL_DestroyRenderer(renderer);
    renderer = NULL;

    // Détruit la fenêtre
    SDL_DestroyWindow(window);
    window = NULL;

    // Libère les polices
    TTF_CloseFont(globalFont);
    TTF_CloseFont(menuFont);

    // Ferme les libs
    IMG_Quit();
    TTF_Quit();
    Mix_Quit();
    SDL_Quit();
}



/******************************************************************************/
/** Boucles de jeu */
/******************************************************************************/

void SDL2_Frontend::menu() {
    bool quit = false;
    bool choiceMade = false;
    mode mode = debug;

    // Création des 3 boutons de mode de jeu
    Button btnNormal;
    btnNormal.label = createTextTexture("Mode normal", menuFont);
    btnNormal.destRect = getDestRect(btnNormal.label, Vector2(220, 250));

    Button btnFast;
    btnFast.label = createTextTexture("Mode rapide", menuFont);
    btnFast.destRect = getDestRect(btnFast.label, Vector2(220, 300));

    Button btnDebug;
    btnDebug.label = createTextTexture("Mode debug", menuFont);
    btnDebug.destRect = getDestRect(btnFast.label, Vector2(220, 350));

    // Boucle répétée tant que l'app fonctionne
    while (!quit) {
        // Lance la musique
        Mix_PlayMusic(musicMenu, -1);

        // Boucle répétée pour choix du mode d'UNE partie
        while (!choiceMade && !quit) {
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    quit = true;
                }
                if (event.type == SDL_MOUSEBUTTONDOWN) {
                    int x = event.motion.x;
                    int y = event.motion.y;

                    // Récupère le mode de jeu selon le bouton cliqué
                    if (btnNormal.onButton(x, y)) {
                        mode = normal;
                        choiceMade = true;
                    }
                    else if (btnFast.onButton(x, y)) {
                        mode = fast;
                        choiceMade = true;
                    }
                    else if (btnDebug.onButton(x, y)) {
                        mode = debug;
                        choiceMade = true;
                    }
                }
            }

            SDL_RenderClear(renderer);

            drawMenuBackground();

            // Affiche les 3 boutons
            SDL_RenderCopy(renderer, btnNormal.label, NULL, &btnNormal.destRect);
            SDL_RenderCopy(renderer, btnFast.label, NULL, &btnFast.destRect);
            SDL_RenderCopy(renderer, btnDebug.label, NULL, &btnDebug.destRect);

            SDL_RenderPresent(renderer);
        }

        // Stop musique
        Mix_HaltMusic();

        // Si un choix à été fait
        if (choiceMade) {
            start(mode);

            // Choice made à FAUX pour reprendre une partie ensuite
            choiceMade = false;
        }
    }

    // Détruit les textures des 3 boutons
    SDL_DestroyTexture(btnNormal.label);
    SDL_DestroyTexture(btnFast.label);
    SDL_DestroyTexture(btnDebug.label);
}

void SDL2_Frontend::start(mode mode) {
    if (mode == debug) {
        game = new Game("Alice", "Bob");
        game->start(mode);

        // En mode débug boucle directement sur une partie en cours
        loop();
    }
    else {
        game = new Game("Joueur 1", "Joueur 2");
        game->start(mode);
        startLoop();
    }
}

void SDL2_Frontend::startLoop() {

SDL_Event event;
    bool quit = false;
    //bool movingPiece = false;
    //Vector2 mouseOffset;
    //test :::SDL_Texture *pieceTest = getPieceTexture(blue,3);
    Vector2 clickedSquare;
    //test ::: SDL_Rect destRectTest = getDestRect(pieceTest, Vector2(450, 350));


    while (!quit) {

            while (SDL_PollEvent(&event)) {
            Piece* piece;
        //    Player* player = game->getCurrentPlayer();

                switch (event.type) {

                case SDL_QUIT: {
                    quit = true;
                    break;
                }

                case SDL_MOUSEBUTTONDOWN: {
                    state.clickedSquare =
                                getClickedSquareStart(event.motion.x, event.motion.y);

                    const bool validSquare = state.clickedSquare != nullVector;
                   // const bool notMovingAPiece = !state.movingPiece;
                    if (validSquare) {
                        piece = getPieceFromStart(game->getCurrentPlayer(),state.clickedSquare);//renvoie le piece cliqué
                        state.movingPiece = true;
                            state.movingPiece = true;
                    }
                    break;
                }

                case SDL_MOUSEBUTTONUP: {
                    if (state.movingPiece == true) {
                        state.clickedSquare =
                            getClickedSquare(event.motion.x, event.motion.y);
                        Piece* nullPiece =
                            game->getPieceFromSquare(state.clickedSquare);

                        // si case vide
                        if (nullPiece == NULL) {
                                game->placePiece(piece, state.clickedSquare);
                                state.moveDone = true;

                                state.movingPiece= false;
                    }

                }
                    break;
            }
                case SDL_KEYDOWN: {
                    // Touche entrée
                    if ( event.key.keysym.scancode == SDL_SCANCODE_RETURN ) {
                        state.clean();
                        loop();
                    }

                    // Touche entrée
                    if ( event.key.keysym.scancode == SDL_SCANCODE_ESCAPE ) {
                        state.clean();
                        game->nextRound();

                    }
                    break;
                }
                }

             SDL_RenderClear(renderer);

            // Plateau en fond
            drawBoard();

            //SDL_RenderCopy(renderer, pieceTest, NULL, &destRectTest);


            drawPiecesStart(game->getCurrentPlayer());
            drawClickedSquare(state.clickedSquare);
            drawPieces(game->getCurrentPlayer());


            SDL_RenderPresent(renderer);


        }

    }
}


void SDL2_Frontend::loop() {
    // Constantes pour 60 images/s
    //const int FRAMERATE = 60;
    //const int TICKS_PER_FRAME = 1000 / FRAMERATE;

    SDL_Event event;

    bool quit = false;

    Vector2 clickedSquare;

    Mix_PlayMusic(musicGame, -1);

    while (!quit) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT: {
                    quit = true;
                    break;
                }

                case SDL_KEYDOWN: {
                    // Touche entrée
                    if ( event.key.keysym.scancode == SDL_SCANCODE_RETURN ) {
                        state.clean();
                        game->nextRound();
                    }
                    break;
                }

                case SDL_MOUSEBUTTONDOWN: {
                    // Récupère la case cliquée
                    state.clickedSquare =
                        getClickedSquare(event.motion.x, event.motion.y);

                    const bool validSquare = state.clickedSquare != nullVector;
                    const bool notMovingAPiece = !state.movingPiece;
                    if (validSquare && notMovingAPiece) {
                        Piece* piece =
                            game->getPieceFromSquare(state.clickedSquare);

                        // Si un mouvement à été fait
                        if (state.moveDone) {
                            // On doit utiliser la même pièce
                            if (piece == game->getCurrentPiece()) {
                                state.movingPiece = true;
                            }
                        }
                        else if (game->pieceCanBeSelected(piece)) {
                            game->setCurrentPiece(piece);
                            state.movingPiece = true;
                            state.moveDone = false;
                        }
                    }
                    break;
                }

                case SDL_MOUSEBUTTONUP: {
                    if (state.movingPiece == true) {
                        // Si on bouge un pion, on vérifie si il peut attaquer
                        // ou bouger sur cette case

                        state.clickedSquare =
                            getClickedSquare(event.motion.x, event.motion.y);

                        Piece* piece =
                            game->getCurrentPiece();
                        Piece* enemy =
                            game->getPieceFromSquare(state.clickedSquare);

                        // si case vide
                        if (enemy == NULL && !state.moveDone) {
                            if (piece->canMove(state.clickedSquare, game)) {
                                game->movePiece(piece, state.clickedSquare);

                                // Mouvement fait -> seule cette pièce peut
                                // encore attaquer ce tour
                                state.moveDone = true;

                                // Si la pièce ne peut pas attaquer, fin du tour
                                if (!(game->hasAttackOptions(piece))) {
                                    state.clean();
                                    game->nextRound();
                                }
                            }
                        }
                        else {
                            if (piece->attackResults(enemy) != error) {
                                // Met à jour State
                                state.attackDone = true;
                                state.setOpponent(0, new Piece(*piece));
                                state.setOpponent(1, new Piece(*enemy));

                                // Joue le son de bataille
                                Mix_PlayChannel(-1, battleSound, 0);

                                // Réalise l'attaque
                                game->attackPiece(piece, state.clickedSquare);

                                // Passe au tour suivant
                                state.clean();
                                game->nextRound();
                            }
                        }
                        state.movingPiece= false;
                    }
                    break;
                }
            }
        } // Fin poll event

        /* Affichage */

        // Reset renderer
        SDL_RenderClear(renderer);

        // Plateau en fond
        drawBoard();

        // Affiche la case cliquée
        drawClickedSquare(state.clickedSquare);

        // Affiche la dernière bataille
        if (state.attackDone) {
            drawBattle(state.opponents[0], state.opponents[1]);
        }

        // Si une pièce en mouvement
        if (state.movingPiece) {
            drawPiecesExceptOne(game->getCurrentPlayer(),
                                game->getCurrentPiece());

            // Dessine la pièce en mouvement
            drawPieceAbsoluteCoords(game->getCurrentPiece(),
                                    Vector2(event.motion.x, event.motion.y));
        }
        // Sinon dessine tout le plateau
        else {
            drawPieces(game->getCurrentPlayer());
        }


        SDL_RenderPresent(renderer);

        // Test si partie finie
        if (game->gameOver()) {
            quit = true;
        }
    }

    Mix_HaltMusic();

    // Test encore si partie finie pour permettre de quitter le jeu en cours
    // Avec retour au menu
    if (game->gameOver()) {
        endLoop(game->getWinner());
    }
}


void SDL2_Frontend::endLoop(Player* winner) {
    bool quit = false;

    Mix_PlayMusic(musicVictory, 1);

    while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            }
        }

        SDL_RenderClear(renderer);

        // Affiche le plateau
        drawBoard();

        // Affiche toutes les pièces pour voir l'état du plateau
        drawPieces(NULL);

        drawWinnerName(winner);

        SDL_RenderPresent(renderer);
    }

    Mix_HaltMusic();
}



/******************************************************************************/
/** Draw */
/******************************************************************************/

void SDL2_Frontend::drawMenuBackground() {
    SDL_RenderCopy(renderer, menuTexture, NULL, NULL);

    SDL_Rect logoRect = getDestRect(logoTexture, Vector2(100, 20));
    // Correction width, height
    logoRect.w = 430;
    logoRect.h = 200;
    SDL_RenderCopy(renderer, logoTexture, NULL, &logoRect);
}

void SDL2_Frontend::drawBoard() {
    SDL_RenderCopy(renderer, boardTexture, NULL, NULL);

    SDL_Rect logoRect = getDestRect(logoTexture, Vector2(450, 20));
    // Correction width, height
    logoRect.w = 160;
    logoRect.h = 80;
    SDL_RenderCopy(renderer, logoTexture, NULL, &logoRect);
}


void SDL2_Frontend::drawPiece(Player* player,
                              Piece* piece,
                              const Vector2& square) {
    SDL_Texture* texture = NULL;

    // Si il y a un pion
    if (piece != NULL) {
        Player* piecePlayer = piece->getPlayer();

        // Si le pion appartient à un joueur on peut le dessiner
        if (piecePlayer != NULL) {
            // Si la pièce est au joueur ou que plaer n'est pas défini
            if (player == NULL || piecePlayer == player) {
                texture = getPieceTexture(getPlayerColor(piecePlayer),
                                          piece->getValue());
            }
            // Sinon la pièce est à l'adversaire et player défini
            else {
                texture = getPieceTexture(getPlayerColor(piecePlayer), 0);
            }
        }
    }

    SDL_Rect destRect = calculateDestRect(square);
    SDL_RenderCopy(renderer, texture, NULL, &destRect);
}


void SDL2_Frontend::drawPieces(Player* player) {
    // Parcours de toutes les cases
    for (int row = 1; row <= 10; row++) {
        for (int col = 1; col <= 10; col++) {
            Vector2 square(col, row);

            // Contenu de la case col, row
            Piece* piece = game->getPieceFromSquare(square);
            drawPiece(player, piece, square);
        }
    }
}



void SDL2_Frontend::drawPiecesStart(Player* player) {
    SDL_Texture* texture = NULL;
    int index = 0;
//    bool exit = false;
    if(player->getNumberOfPieces()==10){
        for (int col = 1; col <= 1; col++) {
            for (int row = 1; row <= 10; row++) {
                Piece* piece = player->getPiece(index);//les pieces sont indexées de 0 à 40
            //afficher que les vectors qui ont une position nulle
            if(piece->getPosition()==nullVector){
                Vector2 square(col, row);
                texture = getPieceTexture(getPlayerColor(player), piece->getValue());
                SDL_Rect destRect = calculateDestRectStart(square);
                SDL_RenderCopy(renderer, texture, NULL, &destRect);

            }
                index += 1; // on passe à la piece suivante
                }
            }
        }
        else {
            for (int col = 1; col <= 4; col++) {
        for (int row = 1; row <= 10; row++) {

                Piece* piece = player->getPiece(index); //les pieces sont indexées de 0 à 40
                Vector2 square(col, row);
                texture = getPieceTexture(getPlayerColor(player), piece->getValue());
                SDL_Rect destRect = calculateDestRectStart(square);
                SDL_RenderCopy(renderer, texture, NULL, &destRect);
                index += 1; // on passe à la piece suivante

            }
        }
    }
}

 Piece* SDL2_Frontend::getPieceFromStart(Player* player,const Vector2& pos) const{

    switch(pos.x){//en fonction de la colonne

        case 1:{
            return player->getPiece(pos.y-1);
        }

        case 2:{
            return player->getPiece(10+(pos.y-1));
        }

        case 3:{
            return player->getPiece(20+(pos.y-1));
        }

        case 4:{
            return player->getPiece(30+(pos.y-1));
        }
        default:{
            exit(1);
        }
    }

}




void SDL2_Frontend::drawPiecesExceptOne(Player* player, Piece* piece) {
    for (int row = 1; row <= 10; row++) {
        for (int col = 1; col <= 10; col++) {
            Vector2 square(col, row);
            // Contenu de la case col, row
            Piece* pieceSquare = game->getPieceFromSquare(square);
            if (piece != pieceSquare) {
                drawPiece(player, pieceSquare, square);
            }
        }
    }
}

void SDL2_Frontend::drawPieceAbsoluteCoords(Piece* piece,
                                            const Vector2& coords) {
    assert(piece != NULL);

    color pieceColor = getPlayerColor(piece->getPlayer());

    SDL_Texture* texture = getPieceTexture(pieceColor, piece->getValue());

    // récupération taille texture
    int w, h;
    SDL_QueryTexture(texture, NULL, NULL, &w, &h);

    // Calcul du rectangle de destination selon les coordonnées absolues
    SDL_Rect destRect;
    destRect.x = coords.x;
    destRect.y = coords.y;
    destRect.w = w;
    destRect.h = h;

    SDL_RenderCopy(renderer, texture, NULL, &destRect);
}


void SDL2_Frontend::drawClickedSquare(const Vector2& clickedSquare) {

    std::stringstream coordsString;
    coordsString << "(" << clickedSquare.x << ", " << clickedSquare.y << ")";
    SDL_Texture* texture = createTextTexture(coordsString.str(), globalFont);

    SDL_Rect destRect = getDestRect(texture, Vector2(500, 400));

    SDL_RenderCopy(renderer, texture, NULL, &destRect);
}


void SDL2_Frontend::drawWinnerName(const Player* winner) {
    std::stringstream str;
    str << winner->getName() << " a gagne !";
    SDL_Texture* texture = createTextTexture(str.str(), globalFont);

    SDL_Rect destRect = getDestRect(texture, Vector2(465, 200));

    SDL_RenderCopy(renderer, texture, NULL, &destRect);
}


void SDL2_Frontend::drawBattle(Piece* piece, Piece* enemy) {
    int x = 480;
    int y = 100;
    int pieceWidth = getPieceTextureWidth();

    drawPieceAbsoluteCoords(piece, Vector2(x, y));
    drawPieceAbsoluteCoords(enemy, Vector2(x + pieceWidth + 10, y));
}

/******************************************************************************/
/** Get & set différents paramètres */
/******************************************************************************/

Player* SDL2_Frontend::getBluePlayer() {
    return game->getPlayer(0);
}

Player* SDL2_Frontend::getRedPlayer() {
    return game->getPlayer(1);
}

enum color SDL2_Frontend::getPlayerColor(Player* player) {
    if (player == getBluePlayer()) {
        return blue;
    }
    else {
        return red;
    }
}

color SDL2_Frontend::getPieceColor(Piece* piece) {
    return getPlayerColor(piece->getPlayer());
}

SDL_Texture* SDL2_Frontend::getPieceTexture(enum color color, int value) {
    assert(color == red || color == blue);
    assert(value >= 0 && value <= 12);

    if (color == blue) {
        return bluePieces[value];
    }
    else {
        return redPieces[value];
    }
}


int SDL2_Frontend::getPieceTextureWidth() {
    int width, height;
    SDL_QueryTexture(bluePieces[0], NULL, NULL, &width, &height);

    return width;
}

/******************************************************************************/
/** Manipulations texture, images & sons */
/******************************************************************************/

SDL_Texture* SDL2_Frontend::textureFromSurface(SDL_Surface* surface) const {
    SDL_Texture* texture;

    texture = SDL_CreateTextureFromSurface(renderer, surface);
    if (texture == NULL) {
        std::cout << "Error creating texture "<< SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }

    SDL_FreeSurface(surface);
    surface = NULL;

    return texture;
}


SDL_Texture* SDL2_Frontend::loadImage(const char* imagePath) {
    SDL_Surface* surface = NULL;

    // Image -> surface
    surface = IMG_Load(imagePath);
    if (surface == NULL) {
        std::cout << "Unable to load image " << IMG_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }

    return textureFromSurface(surface);
}


SDL_Texture* SDL2_Frontend::loadImageWithTransparentBg(const char* imagePath,
                                                       int r,
                                                       int g,
                                                       int b) {
    SDL_Surface* surface = NULL;
    //Charge l'image
    surface = IMG_Load(imagePath);
    if (surface == NULL) {
        std::cout << "Unable to load image " << IMG_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }

    // Rend la couleur transparente
    SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, r, g, b));

    return textureFromSurface(surface);


}


SDL_Texture* SDL2_Frontend::createTextTexture(const std::string text,
                                              TTF_Font* font) {
    SDL_Surface* s = NULL;
    s = TTF_RenderText_Blended(font, text.c_str(), {0, 0, 0});
    return textureFromSurface(s);
}


void SDL2_Frontend::loadPiecesTextures() {
    std::string base_path = "data/";

    for (int i = 0; i <= 12; i++) {
        std::stringstream path1;
        path1 << base_path << "pion_bleu_" <<  i << ".bmp";
        bluePieces.push_back(
            loadImageWithTransparentBg(path1.str().c_str(), 0, 255, 0));

        std::stringstream path2;
        path2 << base_path << "pion_rouge_" << i << ".bmp";
        redPieces.push_back(
            loadImageWithTransparentBg(path2.str().c_str(), 0, 255, 0));

    }
}

Mix_Music* SDL2_Frontend::loadMusic(const char* path) {
    Mix_Music* music;
    music = Mix_LoadMUS(path);
    if (music == NULL) {
        std::cerr << "Unable to load music : " << Mix_GetError() << std::endl;
        exit(1);
    }

    return music;
}

Mix_Chunk* SDL2_Frontend::loadSoundEffect(const char* path) {
    Mix_Chunk* sample;
    sample = Mix_LoadWAV(path);
    if (sample == NULL) {
        std::cerr << "Unable to load sample : " << Mix_GetError() << std::endl;
        exit(1);
    }

    return sample;
}

/******************************************************************************/
/** Coordonnées, cases et calcul de rectangles de destination */
/******************************************************************************/

Vector2 SDL2_Frontend::getClickedSquareGlobal(int x,
                                              int y,
                                              Board _board) const {

    Vector2 square;
    square.x = (x - _board.upperLeftCorner.x) / _board.squareLength + 1;
    square.y = (y - _board.upperLeftCorner.y) / _board.squareLength + 1;

    const bool squareIsOnGrid = square.x < 0 ||
        square.x > _board.numberOfSquareOnX ||
        square.y < 0 ||
        square.y > _board.numberOfSquareOnY;

    if (squareIsOnGrid) {
        return Vector2(-1, -1);
    }
    else {
        return square;
    }
}

Vector2 SDL2_Frontend::getClickedSquare(int x, int y) const {
    return getClickedSquareGlobal(x, y, board);
}


Vector2 SDL2_Frontend::getClickedSquareStart(int x, int y) const {
    return getClickedSquareGlobal(x, y, boardStart);
}


Vector2 SDL2_Frontend::getSquareCornerGlobal(const Vector2& coords,
                                             int squareLength,
                                             const Vector2& upperLeftCorner)
const {
    Vector2 corner;
    corner.x = upperLeftCorner.x + (coords.x - 1) * squareLength;
    corner.y = upperLeftCorner.y + (coords.y - 1) * squareLength;

    return corner;
}


Vector2 SDL2_Frontend::getSquareCorner(const Vector2& coords) const {
    return getSquareCornerGlobal(coords,
                                 board.squareLength,
                                 board.upperLeftCorner);
}


Vector2 SDL2_Frontend::getSquareCornerStart(const Vector2& coords) const {
    return getSquareCornerGlobal(coords,
                                 boardStart.squareLength,
                                 boardStart.upperLeftCorner);
}


SDL_Rect SDL2_Frontend::getDestRect(SDL_Texture* texture,
                                    const Vector2& upperLeftCorner) {
    SDL_Rect destRect;

    // Récupère hauteur et largeur de la texture
    int width, height;
    SDL_QueryTexture(texture, NULL, NULL, &width, &height);

    // Créé le rectangle de destination en respectant la taille de la texture
    destRect.x = upperLeftCorner.x;
    destRect.y = upperLeftCorner.y;
    destRect.w = width;
    destRect.h = height;

    return destRect;
}


SDL_Rect SDL2_Frontend::calculateDestRect(const Vector2& coords) const {
    // Coin de la case de destination
    Vector2 squareCorner = getSquareCorner(coords);

    // Calcul d'un rectangle qui occupe 80% de la case
    SDL_Rect destRect;
    destRect.x = squareCorner.x;
    destRect.y = squareCorner.y;
    destRect.w = board.squareLength - 2;
    destRect.h = board.squareLength - 2;

    return destRect;
}


SDL_Rect SDL2_Frontend::calculateDestRectStart(const Vector2& coords) const {
    // Coin de la case de destination
    Vector2 squareCornerStart = getSquareCornerStart(coords);

    // Calcul d'un rectangle qui occupe 80% de la case
    SDL_Rect destRect;
    destRect.x = squareCornerStart.x;
    destRect.y = squareCornerStart.y;
    destRect.w = boardStart.squareLength;
    destRect.h = boardStart.squareLength;

    return destRect;

}

/******************************************************************************/
/** Autres */
/******************************************************************************/

void SDL2_Frontend::regressionVisual() {
    Vector2 corner = getSquareCorner(Vector2(5, 2));

    // Colorie le coin de la case en rouge
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderDrawPoint(renderer, corner.x, corner.y);
}
