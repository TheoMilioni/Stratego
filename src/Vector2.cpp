#include "Vector2.h"

Vector2::Vector2() {
    x = 0;
    y = 0;
}

Vector2::Vector2(const int _x, const int _y) {
    x = _x;
    y = _y;
}

Vector2::Vector2(const Vector2& v) {
    x = v.x;
    y = v.y;
}

Vector2& Vector2::operator =(const Vector2& v) {
    x = v.x;
    y = v.y;

    return *this;
}

bool Vector2::operator ==(const Vector2& v) const {
    return (x == v.x && y == v.y);
}

bool Vector2::operator !=(const Vector2& v) const {
    return !(*this == v);
}
