#include "Piece.h"

#include <iostream>
#include <cassert>
#include "Game.h"
#include "Player.h"

#include "Vector2.h"

Piece::Piece() {
    value = 0;
    position = Vector2(-1, -1);
    player = NULL;
}

Piece::Piece(int _value, const Vector2& _position, Player* _player) {
    value = _value;
    position = _position;
    player = _player;
}

Piece::Piece(const Piece& piece) {
    value = piece.getValue();
    position = piece.getPosition();
    player = piece.getPlayer();
}

Piece::~Piece() {
    player = NULL;
}

int Piece::getValue() const {
    return value;
}

Player* Piece::getPlayer() const {
    return player;
}

Vector2 Piece::getPosition() const {
    return position;
}

void Piece::setPosition(const Vector2& _position) {
    assert(_position.x >= 1 && _position.x <= 10);
    assert(_position.y >= 1 && _position.y <= 10);

    position = _position;
}

status Piece::defaultAttack(const Piece* enemy) {
    assert(enemy != NULL); // A tester dans les Piece.attaque()

    if (enemy->getPlayer() == player) {
        return error;
    }

    int v = enemy->getValue();

    // Case vide ou obstacle
    if (v == -1) {
        return error;
    }
    // Drapeau ou enemy plus faible
    else if (v == 12 || v < value) {
        return victory;
    }
    // Bombe
    else if (v == 11) {
        return defeat;
    }
    // Ennemi de même niveau
    else if (v == value) {
        return equality;
    }
    // Ennemi plus fort
    else {
        return defeat;
    }
}

status Piece::attackResults(const Piece* enemy) {
    if (enemy == NULL) {
        return error;
    }
    else {
        return defaultAttack(enemy);
    }
}

bool Piece::canMove(const Vector2& _position, const Game* game) {
    return (
        _position.x >= 1 && _position.x <= 10
        && _position.y >= 1 && _position.y <= 10
        && _position.y >= 1
        && _position.y <= 10
        && ((position.x == _position.x
                && (_position.y == position.y + 1 || _position.y == position.y - 1))
            || (position.y == _position.y
                && (_position.x == position.x + 1 || _position.x == position.x - 1))
            )
    );
}

void Piece::regression() {
    // vérifie (après initialisation) si la valeur et la position d'un pion correspondent
    Piece p1(6, Vector2(5, 5), NULL);
    assert(p1.getValue() == 6);
    assert(p1.getPosition() == Vector2(5, 5));

    Player alice("Alice");
    Player bob("Bob");
    Piece p5(5, Vector2(3, 4), &alice);
    Piece p7(7, Vector2(3, 5), &bob);

    assert(p5.canMove(Vector2(2, 4), NULL));
    assert(p5.attackResults(&p7) == defeat);
    assert(p7.attackResults(&p5) == victory);
}

Sapper::Sapper(const Vector2& _position, Player* _player) :
    Piece(3, _position, _player) {}

status Sapper::attackResults(const Piece* enemy) {
    if (enemy == NULL) {
        return error;
    }
    else {
        if (enemy->getValue() == 11) {
            return victory;
        }
        else {
            return defaultAttack(enemy);
        }
    }
}

Scout::Scout(const Vector2& _position, Player* _player) :
    Piece(2, _position, _player) {}

bool Scout::canMove(const Vector2& _position, const Game* game) {
    assert(game != NULL);

    int x = _position.x;
    int y = _position.y;

    Vector2 pos = getPosition();

    // Coordonnées sur le plateau
    const bool onBoard = (x >= 1) && (x <= 10) && (y >= 1) && (y <= 10);
    if (onBoard) {
        bool lineIsEmpty;
        // Déplacement sur la même colonne y
        if (x == pos.x) {
            lineIsEmpty = true;
            if (y > pos.y) {
                // Ligne completement vide
                for (int i = pos.y; i <= y; i++) {
                    if (game->getPieceFromSquare(Vector2(i, x)) != NULL) {
                        lineIsEmpty = false;
                    }
                }
            }
            else {
                for (int i = pos.y; i >= y; i--) {
                    if (game->getPieceFromSquare(Vector2(i, x)) != NULL) {
                        lineIsEmpty = false;
                    }
                }
            }
            return lineIsEmpty;
        }
        // Deplacement sur la même ligne x
        if (y == pos.y) {
            lineIsEmpty = true;
            if (x > pos.x) {
                for (int i = pos.x; i <= x; i++) {
                    if (game->getPieceFromSquare(Vector2(y, i)) != NULL) {
                        lineIsEmpty = false;
                    }
                }
            }
            else {
                for (int i = pos.x; i >= pos.x; i--) {
                    if (game->getPieceFromSquare(Vector2(y, i)) != NULL) {
                        lineIsEmpty = false;
                    }
                }
            }
            return lineIsEmpty;
        }
    }

    // Tout les autres cas : diagonales,etc
    return false;
}

Spy::Spy(const Vector2& _position, Player* _player) :
    Piece(1, _position, _player) {}

status Spy::attackResults(const Piece* enemy) {
    if (enemy == NULL) {
        return error;
    }
    else {
        if (enemy->getValue() == 10) {
            return victory;
        }
        else {
            return defaultAttack(enemy);
        }
    }
}

/* Bomb */

Bomb::Bomb(const Vector2& _position, Player* _player) :
    Piece(11, _position, _player) {}

status Bomb::attackResults(const Piece* enemy) {
    return error;
}

bool Bomb::canMove(const Vector2& position, const Game* game) {
    return false;
}

/* Flag */

Flag::Flag(const Vector2& _position, Player* _player) :
    Piece(12, _position, _player) {}

status Flag::attackResults(const Piece* enemy) {
    return error;
}

bool Flag::canMove(const Vector2& position, const Game* game) {
    return false;
}

/* Obstacle */

Obstacle::Obstacle(Vector2 _position) :
    Piece(-1, _position, NULL) {}

Obstacle::~Obstacle() {}

status Obstacle::attackResults(const Piece* enemy) {
    return error;
}

bool Obstacle::canMove(const Vector2& position, const Game* game) {
    return false;
}
