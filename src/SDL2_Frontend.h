#ifndef INCLUDE_SDL2_FRONTEND
#define INCLUDE_SDL2_FRONTEND

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <vector>
#include <string>
#include "Vector2.h"
#include "Enums.h"
#include "Piece.h"

/* Déclaration anticipée */
class Game;
class Player;

/******************************************************************************/
/* Structures */
/******************************************************************************/

/** @brief Structure State, état dans la boucle principale
 *
 * Gère l'état de l'application pendant la boucle de jeu principale
 */
struct State {
    bool movingPiece;       /**< Pièce en mouvement avec la souris */
    bool attackDone;        /**< Attaque réalisée dans le tour */
    bool moveDone;          /**< Mouvement réalisé dans le tour */
    Piece* opponents[2];    /**< Adversaires pour l'attaque */
    Vector2 clickedSquare;  /**< Case cliquée */

    /** Modifie l'adversaire i en effacant l'ancien */
    void setOpponent(int i, Piece* piece);

    /** Réinitialise state pour le tour suivant */
    void clean();
};

/** @brief Structure Board, représente un plateau avec SDL
 *
 * Représente un plateau
 * Soit un tableau avec un coin, un nombre de cases et une taille de case
 */
struct Board {
    Vector2 upperLeftCorner;    /**< Coin haut gauche (convention SDL) */
    int numberOfSquareOnX;      /**< Nombre de case sur l'axe horizontal */
    int numberOfSquareOnY;      /**< Nombre de cases sur l'axe vertical */
    int squareLength;           /**< Taille d'une case (carré) */
};

/** @brief Structure Button, représente un bouton avec SDL
 *
 * Représente un bouton avec son label et sa taille
 */
struct Button {
    SDL_Texture* label;
    SDL_Rect destRect;

    bool onButton(int x, int y);
};

/******************************************************************************/
/* Classe Frontend SDL2 */
/******************************************************************************/

/** @brief Classe SDL2_Frontend, gère l'affichage graphique de l'app
 *
 * Classe gérant l'affichage graphique de l'application
 * Utilise la lib SDL2
 */
class SDL2_Frontend {

/******************************************************************************/
/* Données membres (privées) */
/******************************************************************************/

private:
    SDL_Window* window;         /**< Fenêtre SDL */
    SDL_Renderer* renderer;     /**< Renderer SDL */

    TTF_Font* globalFont;       /**< Police principale */
    TTF_Font* menuFont;         /**< Police du menu */

    /** Paramètre du plateau (texture, coin, longueur) */
    Board board = {Vector2(8, 9), 10, 10, 431 / 10};

    /** Paramètre du plateau de départ (loopStart) */
    Board boardStart = {Vector2(444,9), 4, 10, 172 / 4};

    /* Textures */
    SDL_Texture* boardTexture;              /**< Texture du plateau */
    SDL_Texture* logoTexture;               /**< Texture du logo */
    SDL_Texture* menuTexture;               /**< Texture du menu */
    std::vector<SDL_Texture*> bluePieces;   /**< Textures des pions bleus */
    std::vector<SDL_Texture*> redPieces;    /**< Textures des pions rouges */

    Game* game;     /**< Pointeur vers l'objet Game */

    State state;    /**< Structure gérant l'état de l'app */

    // Sons du jeu (musique et chunck)
    Mix_Music* musicMenu;       /**< Musique du menu */
    Mix_Music* musicGame;       /**< Musique de fond en partie */
    Mix_Music* musicVictory;    /**< Musique de victoire */
    Mix_Chunk* battleSound;     /**< Son de bataille */

/******************************************************************************/
/* Fonctions membres publiques */
/******************************************************************************/

public:
    SDL2_Frontend();    /**< Constructeur */

    ~SDL2_Frontend();   /**< Destructeur */

    /** @brief Démarre le menu de sélection de mode de jeu
     *
     * @return void
     *
     */
    void menu();

/******************************************************************************/
/* Fonctions membres privées */
/******************************************************************************/

private:

/******************************************************************************/
/* Boucles de jeu */
/******************************************************************************/

    /** @brief démarre l'application
     *
     * @param m mode de jeu (Cf Enums.h)
     * @return void
     *
     */
    void start(mode m);

    /** @brief Boucle de jeu principale
     *
     * @return void
     *
     */
    void loop();

    /** @brief Boucle pour le placement des pions de chaque joueur
     *
     * @return void
     *
     */
    void startLoop();

    /** @brief Boucle de fin de jeu
     *
     * @param winner Player*
     * @return void
     * Affiche l'état du plateau à la fin et le nom du gagnant
     */
    void endLoop(Player* winner);

/******************************************************************************/
/* Draw */
/******************************************************************************/

    /** @brief Dessine l'arrière-plan du menu
     *
     * @return void
     *
     */
    void drawMenuBackground();


    /** @brief Dessine le plateau en fond
     *
     * @return void
     *
     */
    void drawBoard();


    /** @brief Dessine une pièce dans une case donnée
     *
     * @param player Player*
     * @param piece Piece*
     * @param square const Vector2&
     * @return void
     * Dessine la pièce du point de vue d'un joueur donnée
     * Si joueur NULL : toujours visible
     */
    void drawPiece(Player* player, Piece* piece, const Vector2& square);


    /** @brief Dessine toutes les pièces du point de vue d'un joueur
     *
     * @param player Player*
     * @return void
     *
     */
    void drawPieces(Player* player);


    /** @brief Dessine toutes les pièces d'un joueur pour les placer
     *
     * @param player Player*
     * @return void
     *
     */
    void drawPiecesStart(Player* player);


    /** @brief Dessine toutes les pièces sauf une
     *
     * @param player Player*
     * @param piece Piece*
     * @return void
     *
     */
    void drawPiecesExceptOne(Player* player, Piece* piece);


    /** @brief Dessine une pièce à un point précis de la fenêtre
     *
     * @param piece Piece*
     * @param coords const Vector2&
     * @return void
     *
     */
    void drawPieceAbsoluteCoords(Piece* piece, const Vector2& coords);


    /** @brief Affiche la case cliquée (pour debug)
     *
     * @param clickedSquare const Vector2&
     * @return void
     *
     */
    void drawClickedSquare(const Vector2& clickedSquare);


    /** @brief Affiche le nom du gagnant
     *
     * @param winner const Player*
     * @return void
     *
     */
    void drawWinnerName(const Player* winner);


    /** @brief Dessine le résultat d'une bataille
     *
     * @param piece Piece*
     * @param enemy Piece*
     * @return void
     *
     */
    void drawBattle(Piece* piece, Piece* enemy);

/******************************************************************************/
/* Get & set différents paramètres */
/******************************************************************************/

    /** @brief Retourne le joueur bleu
     *
     * @return Player*
     *
     */
    Player* getBluePlayer();


    /** @brief Retourne le joueur rouge
     *
     * @return Player*
     *
     */
    Player* getRedPlayer();


    /** @brief Retourne la couleur d'un joueur
     *
     * @param player Player*
     * @return color
     *
     */
    color getPlayerColor(Player* player);


    /** @brief Retourne la couleur d'une pièce
     *
     * @param piece Piece*
     * @return color
     *
     */
    color getPieceColor(Piece* piece);


    /** @brief Retourne la texture d'une pièce selon sa couleur et sa valeur
     *
     * @param color enum color
     * @param value int
     * @return SDL_Texture*
     *
     */
    SDL_Texture* getPieceTexture(enum color color, int value);


    /** @brief Retourne la largeur de la texture d'un pion (en px)
     *
     * @return int
     *
     */
    int getPieceTextureWidth();


/******************************************************************************/
/* Manipulations texture, images et sons */
/******************************************************************************/

    /** @brief Créé une texture à partir d'une surface donnée
     *
     * @param surface SDL_Surface*
     * @return SDL_Texture*
     *
     */
    SDL_Texture* textureFromSurface(SDL_Surface* surface) const;


    /** @brief Créé une texture à partir d'une image
     *
     * @param imgPath const char*
     * @return SDL_Texture*
     *
     */
    SDL_Texture* loadImage(const char* imgPath);


    /** @brief Charge une image avec une couleur de transparence
     *
     * @param imagePath const char*
     * @param int r
     * @param int g
     * @param int b
     * @return SDL_Texture*
     *
     */
    SDL_Texture* loadImageWithTransparentBg(const char* imagePath,
                                            int r,
                                            int g,
                                            int b);


    /** @brief Créé une texture à partir d'un texte
     *
     * @param std::string const
     * @param font TTF_Font*
     * @return SDL_Texture*
     *
     */
    SDL_Texture* createTextTexture(const std::string, TTF_Font* font);


    /** @brief Charge toutes les textures de pièces
     *
     * @return void
     *
     */
    void loadPiecesTextures();


    /** @brief Charge une musique
     *
     * @param path const char*
     * @return Mix_Music*
     *
     */
    Mix_Music* loadMusic(const char* path);


    /** @brief Charge une effet sonore
     *
     * @param path const char*
     * @return Mix_Chunk*
     *
     */
    Mix_Chunk* loadSoundEffect(const char* path);

/******************************************************************************/
/* Coordonnées, cases et calcul de rectangles de destination */
/******************************************************************************/

    /** @brief Retourne la case cliquée pour un plateau donné
     *
     * @param mouseX int
     * @param mouseY int
     * @param board Board
     * @return Vector2
     *
     */
    Vector2 getClickedSquareGlobal(int mouseX, int mouseY, Board board) const;


    /** @brief Retourne la case cliquée dans le plateau
     *
     * @param mouseX int
     * @param mouseY int
     * @return Vector2
     *
     */
    Vector2 getClickedSquare(int mouseX, int mouseY) const;


    /** @brief Retourne la case cliquée dans le choix du pion
     *
     * @param mouseX int
     * @param mouseY int
     * @return Vector2
     *
     */
    Vector2 getClickedSquareStart(int mouseX, int mouseY) const;


    Vector2 getSquareCornerGlobal(const Vector2& coordinates,
                                  int squareLength,
                                  const Vector2& upperLeftCorner) const;


    /** @brief Retourne le pixel de coin haut gauche d'une case
     *
     * @param coordinates const Vector2&
     * @return Vector2
     *
     */
    Vector2 getSquareCorner(const Vector2& coordinates) const;


    /** @brief Retourne le pixel haut gauche pour le choix du pion
     * @param coordinates const Vector2&
     * @return Vector2
     *
     */
    Vector2 getSquareCornerStart(const Vector2& coordinates) const;


    /** @brief Retourne le rectangle de destination d'une texture
     * à partir d'un point
     *
     * @param texture SDL_Texture*
     * @param upperLeftCorner const Vector2&
     * @return SDL_Rect
     *
     */
    SDL_Rect getDestRect(SDL_Texture* texture, const Vector2& upperLeftCorner);


    /** @brief Calcule le rectangle de destination d'un pion pour une case
     *
     * @param coords const Vector2&
     * @return SDL_Rect
     *
     */
    SDL_Rect calculateDestRect(const Vector2& coords) const;


    /** @brief Calcule le rectangle de destination d'un pion (pour le choix)
     *
     * @param coordinates const Vector2&
     * @return SDL_Rect
     *
     */
    SDL_Rect calculateDestRectStart(const Vector2& coordinates) const;

/******************************************************************************/
/* Autres */
/******************************************************************************/

    /** @brief Tests visuels
     *
     * @return void
     *
     */
    void regressionVisual();


    Piece* getPieceFromStart(Player* player,const Vector2& position) const;
};

#endif
