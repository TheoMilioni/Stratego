#ifndef INCLUDE_ENUMS
#define INCLUDE_ENUMS

/** Mode de jeu */
enum mode {normal, fast, debug};

/** Statut à l'issue d'une attaque */
enum status {error, defeat, equality, victory};

/** Couleur de joueur */
enum color {blue, red};

/** équivalent de Vector2 NULL */
const Vector2 nullVector(-1, -1);

#endif
