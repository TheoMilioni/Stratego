#ifndef INCLUDE_PLAYER
#define INCLUDE_PLAYER

#include "Vector2.h"
#include "Enums.h"

#include <string>
#include <vector>

class Piece;

/** @brief Classe Player, représente un joueur avec son nom et ses pions
 *
 */
class Player {
private:
    std::string name;               /**< Nom du joueur */
    std::vector<Piece*> pieces;     /**< Listes des pions */

public:

    /** @brief Constructeur par défaut
     *
     * Créé un joueur nommé Ordi, seulement pour les tests
     *
     */
    Player();



    /** @brief Constructeur avec le nom fourni
     *
     * @param nom std::string
     *
     */
    Player(std::string name);



    /** @brief Destructeur
     *
     * Libère tout les pointeurs sur les pions restants du joueur
     *
     */
    ~Player();



    /** @brief Retourne le nom
     *
     * @return std::string
     *
     */
    std::string getName() const;



    /** @brief Retourne le nombre de pions actuels
     *
     * @return int
     *
     */
    int getNumberOfPieces() const;



    /** @brief Retourne un pion selon son indice dans la liste
     *
     * @param index int
     * @return Piece*
     *
     */
    Piece* getPiece(int index) const;



    /** @brief Remplit les pions selon le mode de jeu
     *
     * Remplit la liste de pion selon le mode Normal ou Rapide
     *
     * @param m mode
     * @return void
     *
     */
    void fillPieces(mode m);



    /** @brief Supprime un pion de la liste des pions
     *
     * @param piece Piece*
     * @return void
     *
     */
    void removePiece(Piece* piece);



    /** @brief Teste si le joueur possède encore son drapeau
     *
     * @return bool
     *
     */
    bool hasFlag() const;



    /** @brief Teste si le joueur a réuni les conditions de défaites
     *
     * @return bool
     *
     */
    bool hasLost() const;



    /** @brief Opérateur d'égalite entre 2 Players
     *
     * @param player const Player&
     * @return bool
     *
     */
    bool operator==(const Player& player) const;



    /** @brief Opérateur d'inégalité
     *
     * @param j const Player&
     * @return bool
     *
     */
    bool operator!=(const Player& player) const;



    /** @brief Tests de régression du module Player
     *
     * @return void
     *
     */
    void regression();
};

#endif
