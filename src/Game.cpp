#include "Game.h"
#include <cassert>
#include "Player.h"
#include "Piece.h"
#include "Vector2.h"

void Game::defaultGameConstructor() {
    // Initialise le plateau avec tout les pointeurs à NULL
    initBoard();
    // Place les obstacles (lacs)
    setObstacles();

    currentPiece = NULL;
}

Game::Game() : obstacle(Vector2(0, 0)) {
    players[0] = new Player;
    players[1] = new Player;

    defaultGameConstructor();
}

Game::Game(std::string name1, std::string name2) : obstacle(Vector2(0, 0)) {
    players[0] = new Player(name1);
    players[1] = new Player(name2);

    defaultGameConstructor();
}

Game::~Game() {
    // Libère les pointeurs sur Player
    for (int i = 0; i < 2; i++) {
        delete players[i];
        players[i] = NULL;
    }

    removeObstacles();

    // Vérifie qu'il ne reste pas de cases occupées
    for (int row = 0; row < 12; row++) {
        for (int col = 0; col < 12; col++) {
            board[row][col] = NULL;
            assert(board[row][col] == NULL);
        }
    }

    delete currentPiece;
    currentPiece = NULL;
}

Player* Game::getPlayer(int i) const {
    // Vérifie que l'indice est possible
    assert(i == 0 || i == 1);

    return players[i];
}

Piece* Game::getPieceFromSquare(const Vector2& pos) const {
    // Coordonnées possibles

    return board[pos.y][pos.x];
}

Vector2 Game::getSquareFromPiece(Piece* piece) const {
    // Parcours le plateau en cherchant la pièce sinon retourne nullVector
    for (int y = 0; y < 12; y++) {
        for (int x = 0; x < 12; x++) {
            if (getPieceFromSquare(Vector2(x, y)) == piece) {
                return Vector2(x, y);
                break;
            }
        }
    }
    return nullVector;
}

void Game::setPieceOnSquare(Piece* piece, const Vector2& pos) {
    assert(pos.x >= 0 && pos.x <= 12);
    assert(pos.y >= 0 && pos.y <= 12);

    board[pos.y][pos.x] = piece;
}

Player* Game::getCurrentPlayer() const {
    return getPlayer(currentPlayerIndex);
}

Player* Game::getCurrentOpponent() const {
    return getPlayer((currentPlayerIndex + 1) % 2);
}

void Game::setCurrentPlayer(int i) {
    assert(i == 0 || i == 1);
    currentPlayerIndex = i;
}

Piece* Game::getCurrentPiece() const {
    return currentPiece;
}

void Game::setCurrentPiece(Piece* piece) {
    // La pièce doit pouvoir agir ! à vérifier avant
    assert(piece != NULL);
    assert(hasOptions(piece));

    currentPiece = piece;
}

void Game::removePiece(Piece* piece) {
    // Pièce jouable
    assert(piece != NULL);
    assert(piece->getPlayer() != NULL);

    // Vide la case occupée par la pièce
    setPieceOnSquare(NULL, getSquareFromPiece(piece));

    // Récupère le joueur et supprime la pièce de sa liste de pions
    Player* player = piece->getPlayer();
    player->removePiece(piece);
}

void Game::placePiece(Piece* piece, const Vector2& square) {
    // Pièce définie
    assert(piece != NULL);
    // Case de destination vide
    assert(getPieceFromSquare(square) == NULL);

    // Change la position de l'objet pion
    piece->setPosition(square);
    // Place le pion sur le plateau
    setPieceOnSquare(piece, square);
}

void Game::movePiece(Piece* piece, const Vector2& square) {
    // Pièce définie
    assert(piece != NULL);
    // Case de destination vide
    assert(getPieceFromSquare(square) == NULL);

    // Vide l'ancienne position
    setPieceOnSquare(NULL, piece->getPosition());

    placePiece(piece, square);
}

void Game::attackPiece(Piece* piece, const Vector2& attackedSquare) {
    assert(piece != NULL);

    Piece* enemy = getPieceFromSquare(attackedSquare);
    status status = piece->attackResults(enemy);
    switch (status) {
        case equality:
            removePiece(piece);
            removePiece(enemy);
            break;
        case defeat:
            removePiece(piece);
            break;
        case victory:
            removePiece(enemy);
            movePiece(piece, attackedSquare);
            break;
        default: // Si pas de retour -> erreur
            std::cerr << "Résultat d'attaque incorrect" << std::endl;
            exit(1);
            break;
    }
}

bool Game::hasMobilityOptions(Piece* piece) {
    assert(piece != NULL);

    // Position de la pièce (doit être valide)
    Vector2 position = piece->getPosition();
    assert(position != nullVector);
    int x = position.x;
    int y = position.y;

    // Test le déplacement dans les 4 directions
    const bool canMoveUp = piece->canMove(Vector2(x, y + 1), this);
    const bool canMoveDown = piece->canMove(Vector2(x , y - 1), this);
    const bool canMoveLeft = piece->canMove(Vector2(x - 1, y), this);
    const bool canMoveRight = piece->canMove(Vector2(x + 1, y), this);

    return (canMoveUp || canMoveDown || canMoveLeft || canMoveRight);
}

bool Game::hasAttackOptions(Piece* piece) {
    assert(piece != NULL);

    // La pièce doit avoir un joueur
    Player* player = piece->getPlayer();
    assert(player != NULL);

    // Position valide (sur plateau)
    Vector2 position = getSquareFromPiece(piece);
    assert(position != nullVector);
    int x = position.x;
    int y = position.y;

    Piece* enemy = NULL;

    // Test l'attaque dans les 4 directions
    enemy = getPieceFromSquare(Vector2(x + 1, y));
    if (piece->attackResults(enemy) != error) return true;

    enemy = getPieceFromSquare(Vector2(x - 1, y));
    if (piece->attackResults(enemy) != error) return true;

    enemy = getPieceFromSquare(Vector2(x, y + 1));
    if (piece->attackResults(enemy) != error) return true;

    enemy = getPieceFromSquare(Vector2(x , y - 1));
    if (piece->attackResults(enemy) != error) return true;

    return false;
}

bool Game::hasOptions(Piece* piece) {
    return hasMobilityOptions(piece) || hasAttackOptions(piece);
}

void Game::nextRound() {
    setCurrentPlayer((currentPlayerIndex + 1) % 2);
    currentPiece = NULL;
}



bool Game::pieceCanBeSelected(Piece* piece) {
    if (piece != NULL) {
        const bool belongToPlayer = piece->getPlayer() == getCurrentPlayer();

        const int v = piece->getValue();
        const bool movable = v != 11 && v != 12 && v != -1;
        if (belongToPlayer && hasOptions(piece) && movable) {
            return true;
        }
    }
    return false;
}

void Game::setObstacles() {
    // Lacs
    for (int y = 5; y <= 6; y++) {
        for (int x = 3; x <= 4; x++) {
            // lac gauche
            board[y][x] = &obstacle;
            // lac droit
            board[y][x + 4] = &obstacle;
        }
    }
}

void Game::removeObstacles() {
    // Lacs
    for (int y = 5; y <= 6; y++) {
        for (int x = 3; x <= 4; x++) {
            // lac gauche
            board[y][x] = NULL;
            // lac droit
            board[y][x + 4] = NULL;
        }
    }
}

void Game::start(mode m) {
    // Remplit les pièces des joueurs selon le mode de jeu
    for (int i = 0; i < 2; i++) {
        players[i]->fillPieces(m);
    }

    // Place les pieces pour débug sinon placé par joueur
    if (m == debug) {
        placePiece(players[0]->getPiece(0), Vector2(2, 8));
        placePiece(players[0]->getPiece(1), Vector2(4, 9));
        placePiece(players[0]->getPiece(2), Vector2(6, 9));
        placePiece(players[0]->getPiece(3), Vector2(8, 10));

        placePiece(players[0]->getPiece(4), Vector2(7, 10));
        placePiece(players[0]->getPiece(5), Vector2(3, 9));
        placePiece(players[0]->getPiece(6), Vector2(9, 7));


        placePiece(players[1]->getPiece(0), Vector2(8, 3));
        placePiece(players[1]->getPiece(1), Vector2(8, 4));
        placePiece(players[1]->getPiece(2), Vector2(7, 2));
        placePiece(players[1]->getPiece(3), Vector2(6, 2));

        placePiece(players[1]->getPiece(4), Vector2(2, 1));
        placePiece(players[1]->getPiece(5), Vector2(8, 2));
        placePiece(players[1]->getPiece(6), Vector2(7, 3));
    }

    setCurrentPlayer(0);
}

bool Game::gameOver() {
    if (getPlayer(0)->hasLost()) {
        return true;
    }
    if (getPlayer(1)->hasLost()) {
        return true;
    }
    return false;
}

Player* Game::getWinner() {
    if (getPlayer(0)->hasLost()) {
        return getPlayer(1);
    }
    else if (getPlayer(1)->hasLost()) {
        return getPlayer(0);
    }
    else {
        return NULL;
    }
}


void Game::regression() {
    // vérifie si setObstacleBoard place des obstacles sur le board
    Game j1;
    j1.setObstacles();
    for (int y1 = 5; y1 <= 6; y1++) {
        for (int x1 = 3; x1 <= 4; x1++) {
            assert (board[y1][x1] == &obstacle);
            assert (board[y1][x1 + 4] == &obstacle);
        }
    }
}



void Game::dumpBoard() const {
    for (int row = 0; row < 12; row++) {
        for (int col = 0; col < 12; col ++) {
            if (board[row][col] == NULL) {
                std::cout << " ";
            }
            else {
                std::cout << "X";
            }
        }
        std::cout << std::endl;
    }
}

void Game::initBoard() {
    for (int row = 0; row < 12; row++) {
        for (int col = 0; col < 12; col++) {
            board[row][col] = NULL;
        }
    }
}
