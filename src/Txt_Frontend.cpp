#include "Txt_Frontend.h"
#include <iostream>
#include <cassert>
#include <unistd.h>
#include <limits>
#include "Game.h"
#include "Player.h"
#include "Piece.h"

Txt_Frontend::Txt_Frontend() {
    game = NULL;
}

Txt_Frontend::~Txt_Frontend() {
    delete game;
    game = NULL;
}



void Txt_Frontend::start(mode m) {
    if (m == debug) {
        std::cout << "On a 'Joueur Alice' et 'Joueur Bob'" << std::endl;
        game = new Game("Alice", "Bob");
        game->start(m);
    }
    else {
        std::string name1, name2;
        std::cout << "Joueur 1, entrez votre nom : ";
        std::cin >> name1;
        std::cout << "Joueur 2, entrez votre nom : ";
        std::cin >> name2;

        game = new Game(name1, name2);
        game->start(m);

        placePieces(game->getPlayer(0));
        clearScreen();
        std::cout << "Joueur 2, appuyez sur une touche pour placer vos pions : ";
        std::cin.get();
        placePieces(game->getPlayer(1));
    }

    loop();
}

void Txt_Frontend::placePieces(Player* player) {
    for (int i = 0; i < player->getNumberOfPieces(); i++) {
        Piece* piece = player->getPiece(i);
        int x, y;
        bool error;

        std::string coordX, coordY;

        clearScreen();
        drawBoard(player);

        std::cout << std::endl;
        std::cout << player->getName() << ", ";
        std::cout << "placez le pion de valeur " << piece->getValue();
        std::cout << std::endl;


        do {
            error = false;
            std::cout << "Coordonnée X (1, 2, ...) : ";
            std::cin >> coordX;

            // Retourne nb correspondant à la lettre
            x = stoi(coordX);
            if (x < 1 || x > 10) {
                error = true;
                std::cout << "Entrez des coordonnées correctes" << std::endl;
            }
        } while (error);

        do {
            error = false;
            if (player == game->getPlayer(0)) {
                std::cout << "Coordonnée Y (G, H, I ou J) : ";
            }
            else {
                std::cout << "Coordonnée Y (A, B, C ou D) : ";
            }
            std::cin >> coordY;

            // Retourne nb correspondant à la chaîne de caracs
            y = coordY[0] - 'A' + 1;
            if (player == game->getPlayer(0)) {
                if (y < 7 || y > 10) {
                    error = true;
                    std::cout << "Entrez des coordonnées valides" << std::endl;
                }
            }
            else {
                if (y < 1 || y > 4) {
                    error = true;
                    std::cout << "Entrez des coordonnées valides" << std::endl;
                }
            }

            if (game->getPieceFromSquare(Vector2(x, y)) != NULL) { // game->X !!!

                error = true;
                std::cout << "Deja occupé, choisissez une autre case" << std::endl;
            }

        } while (error);

        game->placePiece(piece, Vector2(x, y));
    }
}

void Txt_Frontend::drawBoard(const Player* player) {
    std::cout << "STRATEGO" << std::endl;
    std::cout << "========" << std::endl << std::endl;
    std::cout << "X : Obstacle" << std::endl;
    std::cout << "! : Pion ennemi" << std::endl << std::endl;

    // Coordonnées sur l'axe des x
    std::cout << " ";
    for (int i = 1; i <= 10; i++) {
        std::cout << i;
    }
    std::cout << std::endl;

    // Parcours du plateau
    for (int row = 1; row <= 10; row++) {
        for (int col = 0; col <= 10; col++) {
            // Coordonnées sur l'axe des y
            if (col == 0) {
                std::cout << (char)('A' + row - 1);
            }
            else {
                Piece* piece = game->getPieceFromSquare(Vector2(col, row));
                if (piece == NULL) {
                    std::cout << ' ';
                }
                else if (piece->getPlayer() == player) {
                    int v = piece->getValue();
                    if (v >= 1 && v <= 9) {
                        std::cout << v;
                    }
                    else switch (v) {
                        case 10:
                            std::cout << 'M';
                            break;
                        case 11:
                            std::cout << 'B';
                            break;
                        case 12:
                            std::cout << 'D';
                            break;
                        default:
                            break;
                    }
                }
                else if (piece->getValue() == -1)  {
                    std::cout << 'X';
                }
                else {
                    std::cout << '!';
                }
            }
        }
        std::cout << std::endl;
    }
}

void Txt_Frontend::regression() {

}

void Txt_Frontend::loop() {
    bool quit = false;

    while (!quit) {
        Player* player = game->getCurrentPlayer();
        Piece* piece;
        bool error, pieceCanMove, pieceCanAttack;
        do {
            error = false;
            piece = selectPiece(player);                    // renvoie un pion
            pieceCanMove = game->hasMobilityOptions(piece); // renvoie un booleen si le pion a des possibilités de déplacement
            pieceCanAttack = game->hasAttackOptions(piece); // renvoie un booleen si le pion a des possibilités d'attaques

            if (!pieceCanMove && !pieceCanAttack) {         // 1er cas
                std::cout << "Vous ne pouvez-rien faire avec ce pion !";
                std::cout << "Sélectionnez-en un autre " << std::endl;
                error = true;
            }
        } while (error);

        if (pieceCanMove && pieceCanAttack) {               // 2ème cas
            std::vector<std::string> options = {"Deplacer", "Attaquer"};
            int choice = promptMenu(2, "Vous pouvez attaquer et vous deplacez", options);
            if (choice == 1) {
                doMove(player);
            }
            else {
                doAttack(player);
            }
        }
        else if (pieceCanMove) {                            // 3ème cas
            if (promptBool("Voulez-vous vous déplacez ?")) {
                doMove(player);

                // Attaque possible après le déplacement
                if (game->hasAttackOptions(piece)) {
                    if (promptBool("Voulez-vous attaquez ?")) {
                        doAttack(player);
                    }
                }
            }
        }
        else if (pieceCanAttack) {                          // 4ème cas
            if (promptBool("Voulez-vous attaquez ?")) {
                doAttack(player);
            }
        }

        if (game->gameOver()) {
            quit = true;
        }

        clearScreen();
        std::cout << "Joueur suivant, appuyez sur une touche pour commencer : ";
        clearCin();
        std::cin.get();
        game->nextRound();
    }

    endGame(game->getWinner());
}

void Txt_Frontend::endGame(Player* winner) {
    clearScreen();
    std::cout << "BRAVO " << winner->getName() << " !" << std::endl;
    std::cout << "Vous avez gagné." << std::endl;
}

Vector2 Txt_Frontend::promptCoordinates() const {
    bool error;
    std::string coordX, coordY;
    int x, y;

    do {
        error = false;
        std::cout << "Coordonnée X (1, 2, ...) : ";
        std::cin >> coordX;

        // Retourne nb correspondant à la lettre
        x = stoi(coordX);
        if (x < 1 || x > 10) {
            error = true;
            std::cout << "Entrez des coordonnées correctes" << std::endl;
        }
    } while (error);

    do {
        error = false;
        std::cout << "Coordonnée Y (A, B, ...) : ";
        std::cin >> coordY;

        // Retourne nb correspondant à la chaîne de caracs
        y = coordY[0] - 'A' + 1;
        if (y < 1 || y > 10) {
            error = true;
            std::cout << "Entrez des coordonnées correctes" << std::endl;
        }
    } while (error);

    return Vector2(x, y);
}

Piece* Txt_Frontend::selectPiece(const Player* player) {
    clearScreen();
    drawBoard(player);

    bool error;
    Piece* piece;

    std::cout << std::endl;
    std::cout << player->getName() << ", sélectionnez un pion :" << std::endl;
    do {
        error = false;
        Vector2 coordsPiece = promptCoordinates();

        piece = game->getPieceFromSquare(coordsPiece);

        if (piece == NULL) {
            std::cout << "Sélectionnez une case occupée." << std::endl;
            error = true;
        }
        else {
            int v = piece->getValue();

            if (v == -1) {
                std::cout << "Vous avez sélectionné une case où se situe un obstacle, sélectionnez un pion valide." << std::endl;
                error = true;
            }
            else if (v == 11) {
                std::cout << "Vous avez sélectionné une bombe, elle ne peut pas bouger, sélectionnez un pion valide." << std::endl;
                error = true;
            }
            else if (v == 12) {
                std::cout << "Vous avez sélectionné le drapeau, il ne peut pas bouger, sélectionnez un pion valide." << std::endl;
                error = true;
            }
            else if (piece->getPlayer() != player) {
                std::cout << "Sélectionnez un de vos pions." << std::endl;
                error = true;
            }
        }

    } while (error);

    game->setCurrentPiece(piece);
    return piece;
}

void Txt_Frontend::doAttack(Player* player) {
    clearScreen();
    drawBoard(player);

    bool error;
    Piece* piece;
    Vector2 coordsPiece;

    std::cout << std::endl;
    std::cout << "Attaquer une position :" << std::endl;
    do {
        error = false;
        coordsPiece = promptCoordinates();

        piece = game->getPieceFromSquare(coordsPiece);
        if (piece == NULL) {
            std::cout << "Sélectionnez une case occupée." << std::endl;
            error = true;
        }
        else if (piece->getValue() == -1) {
            std::cout << "Sélectionnez un pion valide." << std::endl;
            error = true;
        }
        else if (piece->getPlayer() == player) {
            std::cout << "Sélectionnez plutôt un adversaire !" << std::endl;
            error = true;
        }
    } while (error);

    game->attackPiece(game->getCurrentPiece(), coordsPiece);
}

void Txt_Frontend::doMove(Player* player) {
    clearScreen();
    drawBoard(player);

    bool error;
    Piece* piece;
    Vector2 coordsPiece;

    std::cout << std::endl;
    std::cout << "Choisissez une position où vous déplacer : " << std::endl;
    do {
        error = false;
        coordsPiece = promptCoordinates();

        piece = game->getPieceFromSquare(coordsPiece);
        Piece* pSelected = game->getCurrentPiece();
        if (!pSelected->canMove(coordsPiece, game)) {
            std::cout << "Sélectionnez une position possible." << std::endl;
            error = true;
        }
        else if (piece != NULL) {
            std::cout << "Sélectionnez une case innoccupée." << std::endl;
            error = true;
        }
    } while (error);

    game->movePiece(game->getCurrentPiece(), coordsPiece);
}

void Txt_Frontend::clearScreen() const {
    #ifdef _WIN32
        system("cls");
    #else
        system("clear");
    #endif
}

void Txt_Frontend::clearCin() const {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

bool Txt_Frontend::promptBool(std::string msg, bool yesDefault) const {
    std::cout << msg << " ";
    if (yesDefault) {
        std::cout << "[O/n] ";
    }
    else {
        std::cout << "[o/N] ";
    }

    clearCin();
    char choice = std::cin.get();

    if (yesDefault) {
        return (choice == 'O' || choice == 'o' || choice == '\n');
    }
    else {
        return (choice == 'O' || choice == 'o');
    }
}

int Txt_Frontend::promptMenu(unsigned int nbOptions, std::string title, std::vector<std::string> options) const {
    assert(nbOptions == options.size());

    std::cout << title << " : " << std::endl;

    for (unsigned int i = 1; i <= nbOptions; i++) {
        std::cout << i << ". " << options[i - 1] << std::endl;
    }
    std::cout << "Votre choix ? ";

    bool error;
        char choice;
        do {
            error = false;
            clearCin();
            choice = std::cin.get();

            // Comparaison sur les valeurs ASCII des char
            if (choice < '1' || choice > ('0' + (int)nbOptions)) {
                std::cout << "Choisir une option valide : ";
                error = true;
            }
        } while (error);

    // Valeur numérique de choice
    return (int)(choice - '0');
}
