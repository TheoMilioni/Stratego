#ifndef INCLUDE_PIECE
#define INCLUDE_PIECE

#include "Vector2.h"
#include "Enums.h"
#include <iostream>

class Game;
class Player;

/** @brief Classe Piece, représente un pion
 *
 *  Représente une Piece de valeur n
 *  Les autres classes de pions héritent de Piece et surcharge les méthodes nécessaires
 *  ex : Démineur modifie attaque(), Eclaireur modifie deplace()
 */
class Piece {
private:
    int value;          /**< Valeur du pion */
    Vector2 position;   /**< Position du pion */
    Player* player;     /**< Joueur */

public:
    /** @brief Constructeur par défaut de Piece
     *
     * Piece de valeur 0 : inutilisable
     * Seulement pour les tests de régression
     *
     */
    Piece();



    /** @brief Constructeur de Piece
     *
     * @param value int
     * @param position Vector2
     * @param player Player*
     *
     */
    Piece(int _value, const Vector2& _position, Player* _player);


    /** @brief Constructeur par copie de Piece
     *
     * @param piece const Piece&
     *
     */
    Piece(const Piece& piece);

    /** @brief Destructeur de Piece
     * Rien à libérer, met seulement joueur à NULL
     *
     */
    virtual ~Piece();



    /** @brief Retourne la valeur du pion
     *
     * @return int
     *
     */
    int getValue() const;



    /** @brief Retourne un pointeur vers le joueur
     *
     * @return Player*
     *
     */
    Player* getPlayer() const;



    /** @brief Retourne la position du pion
     *
     * @return Vector2
     *
     */
    Vector2 getPosition() const;



    /** @brief Modifie la position du pion
     *
     * @param position Vector2&
     * @return void
     *
     */
    void setPosition(const Vector2& position);


    /** @brief Test d'attaque et determine le résultat de l'attaque d'un pion
     *
     * @param enemy const Piece*
     * @return status
     *
     */
    status defaultAttack(const Piece* enemy);



    /** @brief Test d'attaque
     * Retourne le status de l'attaque (cf Enums.h)
     *
     * @param enemy const Piece*
     * @return status
     *
     */
    virtual status attackResults(const Piece* enemy);



    /** @brief Test de déplacement : valide ou non
     *
     * @param position const Vector2&
     * @param game Game* = NULL
     * @return bool
     *
     */
    virtual bool canMove(const Vector2& position, const Game* game = NULL);



    /** @brief Tests de régression du module Piece
     *
     * @return void
     *
     */
    void regression();
};

/** @brief Classe Sapper (Démineur), valeur 3
 *
 * Peut attaquer les bombes
 */
class Sapper : public Piece {
public:
    Sapper(const Vector2& position, Player* player);
    status attackResults(const Piece* enemy);

};

/** @brief Classe Scout (Eclaireur), valeur 2
 *
 * Peut se déplacer en ligne
 */
class Scout : public Piece {
public:
    Scout(const Vector2& position, Player* player);
    bool canMove(const Vector2& position, const Game* game);
};

/** @brief Classe Spy (Espionne), valeur 1
 *
 * Peut vaincre un Maréchal (valeur 10)
 */
class Spy : public Piece {
public:
    Spy(const Vector2& position, Player* player);
    status attackResults(const Piece* enemy);
};

/** @brief Classe Bomb (Bombe)
 *
 * Ne peut ni attaquer ni se déplacer
 * Détruit le pion qui l'attaque
 */
class Bomb : public Piece {
public:
    Bomb(const Vector2& position, Player* player);
    status attackResults(const Piece* enemy);
    bool canMove(const Vector2& position, const Game* game = NULL);
};

/** @brief Classe Flag (Drapeau)
 *
 * Ne peut ni attaquer ni se déplacer
 * Le joueur qui s'empare du drapeau adverse gagne la partie
 */
class Flag : public Piece {
public:
    Flag(const Vector2& position, Player* player);
    status attackResults(const Piece* enemy);
    bool canMove(const Vector2& position, const Game* game = NULL);
};

/** @brief Classe Obstacle
 *
 * Ne peut ni attaquer ni se déplacer
 * Rend une case infranchissable
 */
class Obstacle : public Piece {
public:
    Obstacle(Vector2 position);
    ~Obstacle();
    status attackResults(const Piece* enemy);
    bool canMove(const Vector2& position, const Game* game = NULL);
};

#endif
