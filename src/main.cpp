#include <iostream>

#include "Game.h"
#include "Player.h"
#include "Piece.h"
#include "Enums.h"

void drawLine(int n = 40, char c = '=') {
    for (int i = 0; i < n; i++) {
        std::cout << c;
    }
    std::cout << std::endl;
}

int main() {
    std::cout << "Tests de régression :" << std::endl;
    drawLine();

    std::cout << "Tests du module Jeu..." << std::endl;
    Game game("Bob", "Alice");
    game.regression();

    std::cout << "Tests du module Joueur..." << std::endl;
    Player player;
    player.regression();

    std::cout << "Tests du module Pion..." << std::endl;
    Piece piece;
    piece.regression();

    drawLine();
    std::cout << "Tests finis sans erreurs." << std::endl;

    std::cout << "Appuyez sur une touche pour terminer";
    std::cin.get();

    return 0;
}
