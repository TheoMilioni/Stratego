#ifndef INCLUDE_GAME
#define INCLUDE_GAME

#include <string>
#include "Piece.h"
#include "Vector2.h"
#include "Enums.h"

class Player;

/** @brief Classe Game, module principal du coeur du jeu
 */
class Game {
private:
    /** Plateau 10x10 de la forme [y][x] = [row][col] */
    Piece* board[12][12];

    Player* players[2];     /**< Joueurs */
    Obstacle obstacle;      /**< Pion obstacle */

    int currentPlayerIndex; /**< Index du joueur actuel (0 ou 1) */
    Piece* currentPiece;    /**< Pointeur vers la pièce selectionnée */

public:
    /** @brief Constructeur par défaut
     *
     * Initialise un jeu à 2 players par défaut
     * Pour les tests seulement
     *
     */
    Game();



    /** @brief Constructeur avec les noms de 2 players
     *
     * @param name1 std::string
     * @param name2 std::string
     *
     */
    Game(std::string name1, std::string name2);



    /** @brief Destructeur
     * Libère la mémoire des 2 players
     * Nettoie le board et vérifie qu'il ne reste pas de pieces dessus
     *
     */
    ~Game();



    /** @brief Retourne le player 0 ou 1
     *
     * @param i int
     * @return Player*
     *
     */
    Player* getPlayer(int i) const;



    /** @brief Retourne le piece à la case x, y
     *
     * @param position const Vector2&
     * @return Piece*
     *
     */
    Piece* getPieceFromSquare(const Vector2& position) const;

    /** @brief Retourne la position d'un piece dans le board
     * Retourne (-1, -1) si le piece n'est pas trouvé
     * pour tester le retour de la fonction
     *
     * @param piece Piece*
     * @return Vector2
     *
     */
    Vector2 getSquareFromPiece(Piece* piece) const;



    /** @brief Retourne le player dont c'est le tour
     *
     * @return Player*
     *
     */
    Player* getCurrentPlayer() const;



    /** @brief Retourne l'adversaire du player actuel
     *
     * @return Player*
     *
     */
    Player* getCurrentOpponent() const;



    /** @brief Retourne le piece actif (sélectionné par le player actuel)
     *
     * @return Piece*
     *
     */
    Piece* getCurrentPiece() const;



    /** @brief Modifie le player actuel
     *
     * @param index int
     * @return void
     *
     */
    void setCurrentPlayer(int index);



    /** @brief Place un piece sur une case du board
     *
     * @param piece Piece*
     * @param square const Vector2&
     * @return void
     *
     */
    void setPieceOnSquare(Piece* piece, const Vector2& square);



    /** @brief Place le piece dans la case
     *
     * @param piece Piece*
     * @return void
     *
     */
    void setCurrentPiece(Piece* piece);



    /** @brief Passe au tour suivant
     * Modifie le player actuel et remet le piece selectionné à NULL
     *
     * @return void
     *
     */
    void nextRound();



    /** @brief Supprime un piece de la partie
     * Supprime le piece du board et de la liste des pieces du player
     * auquel il appartient
     *
     * @param piece Piece*
     * @return void
     *
     */
    void removePiece(Piece* piece);




    /** @brief Place le piece sur la case (et modifie sa position interne)
     *
     * @param piece Piece*
     * @param square const Vector2&
     * @return void
     *
     */
    void placePiece(Piece* piece, const Vector2& square);



    /** @brief Déplace un piece sur le board
     * Modifie les cases du board en jeu et la position du piece en interne
     * et vide l'ancienne case
     *
     * @param piece Piece*
     * @param square const Vector2&
     * @return void
     *
     */
    void movePiece(Piece* piece, const Vector2& square);



    /** @brief Realise l'attaque d'un piece sur une case
     *
     * @param piece Piece*
     * @param attackedSquare const Vector2&
     * @return void
     *
     */
    void attackPiece(Piece* piece, const Vector2& attackedSquare);



    /** @brief Test si un piece à des possibilités de déplacement
     *
     * @param piece Piece*
     * @return bool
     *
     */
    bool hasMobilityOptions(Piece* piece);



    /** @brief Test si un piece à des possibilités d'attaque
     *
     * @param piece Piece*
     * @return bool
     *
     */
    bool hasAttackOptions(Piece* piece);



    /** @brief Test si un piece à des possibilités d'attaque ou de déplacement
     *
     * @param piece Piece*
     * @return bool
     *
     */
    bool hasOptions(Piece* piece);



    /** @brief Test si un piece peut être selectionnée
     *
     * @param piece Piece*
     * @return bool
     *
     */
    bool pieceCanBeSelected(Piece* piece);



    /** @brief Démarre une partie selon le mode fourni (normal ou rapide)
     *
     * @param m const mode
     * @return void
     *
     */
    void start(const mode m);



    /** @brief Determine si la partie est terminée
     *
     * @return bool
     *
     */
    bool gameOver();



    /** @brief Determine le joueur gagnant
     *
     * @return Player*
     *
     */
    Player* getWinner();



    /** @brief Pour montrer le plateau sur STDOUT
     *
     * @return void
     *
     */
    void dumpBoard() const;



    /** @brief Tests de régression du module Game
     *
     * @return void
     *
     */
    void regression();

private:
 /** @brief Constructeur par défaut
     * Initialise un jeu à 2 players par défaut
     *
     * @return void
     *
     */
    void defaultGameConstructor();



    /** @brief Place les obstacles sur le board (2 lacs au centre)
     *
     * @return void
     *
     */
    void setObstacles();



    /** @brief Remet les cases d'obstacles à NULL
     *
     * @return void
     *
     */
    void removeObstacles();



    /** @brief Initialise le board
     *
     * @return void
     *
     */
    void initBoard();

};

#endif
